const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const cors = require('cors');

const port = process.env.PORT || 3333;
const url = process.env.URL || 'http://localhost';

const app = express();
app.use(cors());

const server = require('http').Server(app);
const io = require('socket.io')(server);

io.on('connection', (socket) => {
  socket.on('connectRoom', (box) => {
    socket.join(box);
  });
});

mongoose
  .connect(
    'mongodb+srv://omnistack:omnistack@cluster0-hpy13.mongodb.net/omnistack?retryWrites=true',
    {
      useNewUrlParser: true,
    },
  )
  .then(() => console.log('Connect successful!'))
  .catch((err) => {
    console.error(`Connect failed! ${err}`);
  });

app.use((req, res, next) => {
  req.io = io;

  return next();
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/files', express.static(path.resolve(__dirname, '..', 'tmp')));

app.use(require('./routes'));

server.listen(port, (err) => {
  if (err) {
    console.log('Error found.', err);
  }

  console.log(`server runnning at ${url}:${port}`);
});
